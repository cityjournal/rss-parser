from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'main.views.home', name='homepage'),
    url(r'^ajax/more/(?P<request_id>\d+)/$', 'main.views.more'),
    url(r'^next/(\d+)/$', 'main.views.gonext'),
    # url(r'^rssparser/', include('rssparser.foo.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)


#if settings.DEBUG:
#    urlpatterns = patterns('',
#    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
#        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
#    url(r'', include('django.contrib.staticfiles.urls')),
#) + urlpatterns