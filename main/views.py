# Create your views here.
from main.models import RssNews
from django.utils import simplejson
from django.views.decorators.cache import cache_page
from django.shortcuts import render, render_to_response, get_object_or_404, HttpResponse
from django.http import HttpResponseRedirect
from django.template.context import RequestContext
import feedparser

limit_per_page = 20

@cache_page(60 * 5)
def home(request):
    rssnodes = RssNews.objects.order_by('-date')[:limit_per_page]
    mainnews = RssNews.objects.order_by('-linkcount')[:6]
    last_id = rssnodes[limit_per_page-1].id
    return render_to_response('home.html', {'rssnodes': rssnodes, 'last_id': last_id, 'mainnews': mainnews}, context_instance=RequestContext(request))


@cache_page(60 * 5)
def more(request, request_id):
    object = RssNews.objects.filter(id__lt = request_id).order_by('-date')[:limit_per_page]
    last_id = object[limit_per_page-1].id
    return render_to_response('ajax_response.html', { 'object': object, 'last_id': last_id })
    
def gonext(request, gonextid):
    object = get_object_or_404(RssNews, id=gonextid)
    object.linkcount = object.linkcount + 1
    object.save()
    return HttpResponseRedirect(object.link)