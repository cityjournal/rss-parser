from django.db import models

# Create your models here.


class RssNode(models.Model):
        title = models.CharField(max_length=150)
        rsslink = models.URLField(unique=True)
        premoderate = models.BooleanField(default=False)
        class Meta:
                verbose_name = u"RSS Node"
                verbose_name_plural = u"RSS Nodes"
        def __unicode__(self):
                return self.title


class RssNews(models.Model):
        title = models.CharField(max_length=150)
        description = models.TextField(blank=True)
        link = models.URLField(unique=True)
        linkcount = models.IntegerField()
        sitename = models.CharField(max_length=150)
        date = models.DateTimeField(auto_now_add=True)
        class Meta:
                verbose_name = u"RSS News"
                verbose_name_plural = u"RSS News"
        def __unicode__(self):
                return self.title
