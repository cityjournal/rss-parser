from django.core.management.base import BaseCommand, CommandError
from django.utils.safestring import SafeString
from main.models import *
import feedparser
from datetime import datetime, timedelta, date

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        i = 0
        rssnodes = RssNode.objects.filter(premoderate=False)
        results = []
        for node in rssnodes:
            results.append(feedparser.parse(node.rsslink))

        for site in results:
    	    if site.feed.title:
                #self.stdout.write('Parse %s ...' % site.feed.title)
        	for entry in site.entries:
            	    News= RssNews(title=entry.title, description=entry.description, sitename=site.feed.title, link=entry.link, linkcount=0)
                    try:
                        News.save()
                        i = i+1
                        self.stdout.write('%s added to database' % entry.title)
                    except:
                        pass
	    else:
		self.stdout.write("Bad request URL  - %s" % site)
        self.stdout.write("Added %d entries" % i)
        deletedate = datetime.today() - timedelta(days=1)
        self.stdout.write(deletedate.__str__())
        fordel=RssNews.objects.filter(date__lt=deletedate).delete()
