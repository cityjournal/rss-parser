from main.models import RssNode, RssNews
from django.contrib import admin

class RssNodeAdmin(admin.ModelAdmin):
    list_display = ('title', 'premoderate')
    ordering = ('-id',)
    list_per_page = 20
    search_fields = ['title']

class RssNewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'sitename', 'date', 'linkcount')
    ordering = ('-id',)
    date_hierarchy = 'date'
    list_filter = ('date',)
    list_per_page = 60
    search_fields = ['title']

admin.site.register(RssNode, RssNodeAdmin)
admin.site.register(RssNews, RssNewsAdmin)